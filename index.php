<?php


  require_once(dirname(dirname(__FILE__)) . '/config.php');
  require_once('lib.php');

  // Setup the page
  $systemcontext = context_system::instance();
  $PAGE->set_url('/the_network/index.php');
  $PAGE->set_context(context_system::instance());
  $PAGE->set_pagelayout('base'); 
  $pagetitle  = "The Network";
  $PAGE->set_title($pagetitle);

  // Some other stuff
  $PAGE->requires->css('/the_network/styles.css');
  $PAGE->requires->css('/the_network/fancybox/source/jquery.fancybox.css?v=2.1.5');

  // Require login
  require_login();

    // Page Output
  echo $OUTPUT->header();

  // ['Connect', -16.5, 62, 'images/connect.png',0,true,,],

  $map_locations =  array();
  $map_locations[0] = array(
        // "scoid"       => 14,
        "title"       => "Connect",
        "lat"         => -16.5,
        "long"        => 62,
        "icon"        => 'images/connect.png',
        "layerviszoom"=> 0,
        "layervisload"=> true,
        //"desc"        => "Communcation game",
        "url"         => $CFG->wwwroot . "/the_network/video/fire_safety_example.mp4" 
      );
  $map_locations[1] = array(
        "scoid"       => 14,
        "title"       => "Communication",
        "lat"         => 1.7,
        "long"        => 48.25,
        "icon"        => 'images/scorm_1_1_incomplete.png',
        "layerviszoom"=> 1,
        "layervisload"=> false,
        //"desc"        => "Communcation game",
        "url"         => $CFG->wwwroot . "/mod/scorm/player.php?a=5&currentorg=Storyline_SCORM_Track_ORG&scoid=14&display=popup&mode=normal" 
      );
  $map_locations[2] = array(
        "scoid"       => 14,
        "title"       => "Communication",
        "lat"         => 1.7,
        "long"        => 52.5,
        "icon"        => 'images/scorm_1_2_incomplete.png',
        "layerviszoom"=> 2,
        "layervisload"=> false,
        //"desc"        => "Communcation game",
        "url"         => $CFG->wwwroot . "/mod/scorm/player.php?a=5&currentorg=Storyline_SCORM_Track_ORG&scoid=14&display=popup&mode=normal" 
      );
  // $map_locations[3] = array(
  //     //"scoid"       => 14,
  //     "title"       => "Communication",
  //     "lat"         => -10,
  //     "long"        => 20,
  //     "icon"        => 'images/scorm_1_2_incomplete.png',
  //     "layerviszoom"=> 0,
  //     "layervisload"=> true,
  //     //"desc"        => "Communcation game",
  //     "url"         => $CFG->wwwroot . "/the_network/video/fire_safety_example.mp4"
  //   );

  $completions = get_module_status($USER->id);
  // echo "<pre>";var_dump($completions);echo "</pre>";

  foreach($map_locations as $key=>$loc){

    if(isset($loc['scoid'])){
     
      foreach($completions as $row){

        if($row->scoid == $loc['scoid'] && $row->value == 'completed'){
          if($loc['layerviszoom'] == 0){
            $map_locations[$key]['icon'] = 'images/scorm_1_0_complete.png'; 
          }else if($loc['layerviszoom'] == 1){
            $map_locations[$key]['icon'] = 'images/scorm_1_1_complete.png';
          }else if($loc['layerviszoom'] == 2){
            $map_locations[$key]['icon'] = 'images/scorm_1_2_complete.png';
          }

        }
        
      }
    }
  }


?>

  </div>
</section>
</div>



</div> <!-- end of page content -->
</div> <!-- end of page -->


      <div id="map"></div> <!-- for this to work these can't be nested in other divs -->
      <div id="coords"></div>
      <div id="key"></div>

      <script>

      // This example defines an image map type using the Gall-Peters projection.
      // https://en.wikipedia.org/wiki/Gall%E2%80%93Peters_projection

    function initMap() {
        // Create a map. Use the Gall-Peters map type.
        var map = new google.maps.Map(document.getElementById('map'), {

        zoom: 0,
        backgroundColor: '#010914',
        center: {lat: 0, lng: 0},
        streetViewControl: false,
        zoomControl: false,
        mapTypeControl: false
    });

    //////////////////Elliot bespoke zoomControl

    // Create the DIV to hold the control and call the ZoomControl() constructor
    // passing in this DIV.
    var zoomControlDiv = document.createElement('div');
    var zoomControl = new ZoomControl(zoomControlDiv, map);

    zoomControlDiv.index = 1;
    map.controls[google.maps.ControlPosition.TOP_RIGHT].push(zoomControlDiv);


    initGallPeters();
    map.mapTypes.set('gallPeters', gallPetersMapType);
    map.setMapTypeId('gallPeters');

    ///////////////

    map.controls[google.maps.ControlPosition.TOP_LEFT].push(document.getElementById('key'));

    // Show the lat and lng under the mouse cursor.
    var coordsDiv = document.getElementById('coords');
    map.controls[google.maps.ControlPosition.TOP_CENTER].push(coordsDiv);
    map.addListener('mousemove', function(event) {
    coordsDiv.textContent =
        'lat: ' + Math.round(event.latLng.lat()) + ', ' +
        'lng: ' + Math.round(event.latLng.lng());
    });

    var infowindow = new google.maps.InfoWindow();
    var marker, i;
    var markers = [];


    var locations = <?php echo json_encode($map_locations); ?> ; // locations defined in PHP above.

    // COPIED ZOOM PERSIST CODE --------------------

  /*google.maps.event.addListener(map, 'tilesloaded', tilesLoaded);
  function tilesLoaded() {
      google.maps.event.clearListeners(map, 'tilesloaded');
      google.maps.event.addListener(map, 'zoom_changed', saveMapState);
      google.maps.event.addListener(map, 'dragend', saveMapState);
  } 

  function saveMapState() { 
    var mapZoom=map.getZoom(); 
    var mapCentre=map.getCenter(); 
    var mapLat=mapCentre.lat(); 
    var mapLng=mapCentre.lng(); 
    var cookiestring=mapLat+"_"+mapLng+"_"+mapZoom; 
    setCookie("myMapCookie",cookiestring, 30); 
  } 

  function loadMapState() { 
      var gotCookieString=getCookie("myMapCookie"); 
      var splitStr = gotCookieString.split("_");
      var savedMapLat = parseFloat(splitStr[0]);
      var savedMapLng = parseFloat(splitStr[1]);
      var savedMapZoom = parseFloat(splitStr[2]);
      if ((!isNaN(savedMapLat)) && (!isNaN(savedMapLng)) && (!isNaN(savedMapZoom))) {
          map.setCenter(new google.maps.LatLng(savedMapLat,savedMapLng));
          map.setZoom(savedMapZoom);
      }
  }

  function setCookie(c_name,value,exdays) {
      var exdate=new Date();
      exdate.setDate(exdate.getDate() + exdays);
      var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
      document.cookie=c_name + "=" + c_value;
  }

  function getCookie(c_name) {
      var i,x,y,ARRcookies=document.cookie.split(";");
      for (i=0;i<ARRcookies.length;i++)
      {
        x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
        y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
        x=x.replace(/^\s+|\s+$/g,"");
        if (x==c_name)
          {
          return unescape(y);
          }
        }
      return "";
  }*/

  

  // END OF COPIED ZOOM PERSIST CODE --------------------


    /* Get the markers from the array */
    for (i = 0; i < locations.length; i++) {  
        marker = new google.maps.Marker({
            position: new google.maps.LatLng(locations[i].lat, locations[i].long), 
            map: map,
            animation: '',
            optimized: false,
            title: locations[i].title,
            visible: locations[i].layervisload,
            icon: locations[i].icon,
            zIndex: 10

        }); 


        /* Open marker on mouseover */
        google.maps.event.addListener(marker, 'mouseover', (function(marker, i) {
        return function() {
            if(locations[i].anim != null){
              marker.setAnimation(google.maps.Animation.BOUNCE);
              setTimeout(function(){ marker.setAnimation(null); }, 300);
            }
            if(locations[i].desc !=null){
              infowindow.setContent(locations[i].desc);
              infowindow.open(map, marker);
            }
          }
        })(marker, i));

        google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          // window.open(locations[i].url);
          launchModule(locations[i].url);

        }
        })(marker, i));

        markers.push(marker); // save all markers 

    }

    /* Change markers on zoom */
    google.maps.event.addListener(map, 'zoom_changed', function() {
      infowindow.close(map, marker);
        var zoom = map.getZoom();

          for (i = 0; i < locations.length; i++) {
            if(locations[i].layerviszoom==zoom){
              markers[i].setVisible(true);
            } else {
              markers[i].setVisible(false);
            }
          } 

    });

    google.maps.event.addListener(map, 'tilesloaded', tilesLoaded);
    function tilesLoaded() {
        google.maps.event.clearListeners(map, 'tilesloaded');
        google.maps.event.addListener(map, 'zoom_changed',  function() {
          var mapZoom=map.getZoom(); 
          var mapCentre=map.getCenter(); 
          var mapLat=mapCentre.lat(); 
          var mapLng=mapCentre.lng(); 
          var cookiestring=mapLat+"_"+mapLng+"_"+mapZoom; 
          setCookie("myMapCookie",cookiestring, 30); 
        });
        google.maps.event.addListener(map, 'dragend', function() {
          var mapZoom=map.getZoom(); 
          var mapCentre=map.getCenter(); 
          var mapLat=mapCentre.lat(); 
          var mapLng=mapCentre.lng(); 
          var cookiestring=mapLat+"_"+mapLng+"_"+mapZoom; 
          setCookie("myMapCookie",cookiestring, 30); 
        });
    } 
    loadMapState(map);

  }


  var gallPetersMapType;
  function initGallPeters() {
    var GALL_PETERS_RANGE_X = 1800;
    var GALL_PETERS_RANGE_Y = 700;

    // Fetch Gall-Peters tiles stored locally on our server.
    gallPetersMapType = new google.maps.ImageMapType({
      getTileUrl: function(coord, zoom) {
        var scale = 1 << zoom;

        // Wrap tiles horizontally.
        var x = ((coord.x % scale) + scale) % scale;

        // Don't wrap tiles horizontally.
        //var x = coord.x;
        //if (x < 0 || x >= scale) return null;

        // Don't wrap tiles vertically.
        var y = coord.y;
        if (y < 0 || y >= scale) return null;

        return 'images/gall-peters_' + zoom + '_' + x + '_' + y + '.jpg';
      },
      tileSize: new google.maps.Size(GALL_PETERS_RANGE_X, GALL_PETERS_RANGE_Y),
      isPng: true,
      minZoom: 0,
      maxZoom: 2,
      name: 'Gall-Peters'
    });

    // Describe the Gall-Peters projection used by these tiles.
    gallPetersMapType.projection = {
      fromLatLngToPoint: function(latLng) {
        var latRadians = latLng.lat() * Math.PI / 180;
        return new google.maps.Point(
            GALL_PETERS_RANGE_X * (0.5 + latLng.lng() / 360),
            GALL_PETERS_RANGE_Y * (0.5 - 0.5 * Math.sin(latRadians)));
      },
      fromPointToLatLng: function(point, noWrap) {
        var x = point.x / GALL_PETERS_RANGE_X;
        var y = Math.max(0, Math.min(1, point.y / GALL_PETERS_RANGE_Y));

        return new google.maps.LatLng(
            Math.asin(1 - 2 * y) * 180 / Math.PI,
            -180 + 360 * x,
            noWrap);
      }
    };
  }

  ////////////////////bespoke zoom from http://jsfiddle.net/maunovaha/jptLfhc8/

function ZoomControl(controlDiv, map) {
  
  // Creating divs & styles for custom zoom control
  controlDiv.style.padding = '5px';

  // Set CSS for the control wrapper
  var controlWrapper = document.createElement('div');
  controlWrapper.style.cursor = 'pointer';
  controlWrapper.style.textAlign = 'center';
  controlWrapper.style.width = '60px'; 
  controlWrapper.style.height = '144px';
  controlDiv.appendChild(controlWrapper);
  
  // Set CSS for the zoomIn
  var zoomInButton = document.createElement('div');
  zoomInButton.style.width = '60px'; 
  zoomInButton.style.height = '69px';
  /* Change this to be the .png image you want to use */
  zoomInButton.style.backgroundImage = 'url("images/zoomIn.png")';
  controlWrapper.appendChild(zoomInButton);
    
  // Set CSS for the zoomOut
  var zoomOutButton = document.createElement('div');
  zoomOutButton.style.width = '60px'; 
  zoomOutButton.style.height = '75px';
  /* Change this to be the .png image you want to use */
  zoomOutButton.style.backgroundImage = 'url("images/zoomOut.png")';

  controlWrapper.appendChild(zoomOutButton);

  // Setup the click event listener - zoomIn
  google.maps.event.addDomListener(zoomInButton, 'click', function() {
    map.setZoom(map.getZoom() + 1);
  });
    
  // Setup the click event listener - zoomOut
  google.maps.event.addDomListener(zoomOutButton, 'click', function() {
    map.setZoom(map.getZoom() - 1);
  });  

  google.maps.event.addListener(map, 'zoom_changed', function() {
    var zoom = map.getZoom();
    if(zoom==0){
      zoomInButton.style.backgroundImage = 'url("images/zoomIn.png")';
      zoomOutButton.style.backgroundImage = 'url("images/zoomOut.png")';
    }
    if(zoom==1){
      zoomInButton.style.backgroundImage = 'url("images/zoomIn.png")';
      zoomOutButton.style.backgroundImage = 'url("images/zoomOut1.png")';
    }
    if(zoom==2){
      zoomInButton.style.backgroundImage = 'url("images/zoomInFade.png")';
      zoomOutButton.style.backgroundImage = 'url("images/zoomOut2.png")';
    }
  });

    
}

function saveMapState() { 
    var mapZoom=map.getZoom(); 
    var mapCentre=map.getCenter(); 
    var mapLat=mapCentre.lat(); 
    var mapLng=mapCentre.lng(); 
    var cookiestring=mapLat+"_"+mapLng+"_"+mapZoom; 
    setCookie("myMapCookie",cookiestring, 30); 
} 

function loadMapState(map) { 
    var gotCookieString=getCookie("myMapCookie"); 
    var splitStr = gotCookieString.split("_");
    var savedMapLat = parseFloat(splitStr[0]);
    var savedMapLng = parseFloat(splitStr[1]);
    var savedMapZoom = parseFloat(splitStr[2]);
    if ((!isNaN(savedMapLat)) && (!isNaN(savedMapLng)) && (!isNaN(savedMapZoom))) {
        map.setCenter(new google.maps.LatLng(savedMapLat,savedMapLng));
        map.setZoom(savedMapZoom);
    }
}

function setCookie(c_name,value,exdays) {
    var exdate=new Date();
    exdate.setDate(exdate.getDate() + exdays);
    var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
    document.cookie=c_name + "=" + c_value;
}

function getCookie(c_name) {
    var i,x,y,ARRcookies=document.cookie.split(";");
    for (i=0;i<ARRcookies.length;i++)
    {
      x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
      y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
      x=x.replace(/^\s+|\s+$/g,"");
      if (x==c_name)
        {
        return unescape(y);
        }
      }
    return "";
}

  </script>



    <script type="text/javascript" src="<?php echo $CFG->wwwroot ; ?>/the_network/fancybox/lib/jquery-1.10.1.min.js"></script>
    <script type="text/javascript" src="<?php echo $CFG->wwwroot ; ?>/the_network/fancybox/source/jquery.fancybox.pack.js?v=2.1.5"></script>
    <script type="text/javascript">
      $(document).ready(function() {
        // $(".fancybox").fancybox();
      });

      function launchModule(url){
        $.fancybox.open([
            {
                href : url,
                title : '1st title',
                type : 'iframe',
                afterClose: function () { // USE THIS IT IS YOUR ANSWER THE KEY WORD IS "afterClose"
                    parent.location.reload(true);
                }

            } 
        ],{
            width    : '90%',
            height   : '90%',
            scrollOutside   : true,
            openEffect  : 'none',
            closeEffect : 'none',
            nextEffect  : 'none',
            prevEffect  : 'none'
        });

      }


    </script>

  <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBQkivWanCrL1JLKc5km8p8ZV2hdr6xn0w&callback=initMap">
  </script>

  <?php echo $OUTPUT->footer(); ?>


</body>
</html>