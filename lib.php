<?php


function get_module_status($userid){
	global $DB;

	// get all modules within The Network course. SCOids hardcoded
	$sql = "
		SELECT sst.id,userid,attempt,scoid,value FROM challenge.mdl_scorm_scoes_track sst
		JOIN mdl_scorm_scoes sco ON sco.id = sst.scoid
		JOIN mdl_scorm s ON s.id = sco.scorm
		WHERE scoid IN (10,12,14)
		AND userid = ?
		AND element = 'cmi.core.lesson_status'";

	$data = $DB->get_records_sql($sql,array($userid));

    return $data;

}


?>